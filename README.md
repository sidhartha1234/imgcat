#### About

imgcat.sh is a simple shell script used to facilitate sorting a set of images. The repository contains a sample configuration file as well.

#### Description

This script allows user to sort all the images in current directory according to categories defined in configuration file. The script goes through all the images one by one, shows them with *feh* and presents a menu contaning the categories specified in config file. User has an option to define the category of an image presented or skip it. The script then creates a symlink for image in a directory named after category. This script uses *dmenu* to create the menu presented to the user.

The format of configuration file is straight forward. Each line of the file defines new sorting category and consists of two fields separated by a colon. The first field contains a letter used as a menu shortcut and the second field contains the name of a category which is also used as the name of a directory in which images of this category will be stored.

#### Usage

This script can be invoked with or without arguments. The following arguments are supported:

+ *-h* prints short help message;
+ *PATH* is optional path to the directory where all subdirectories with simlinks will be stored. If the path is not specified then current directory is used.

