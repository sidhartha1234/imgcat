#!/bin/bash
#
# Name:			imgcat.sh
# Date:			22.11.2015
# Description:	This script is used for sorting images. It shows an image with feh and
#				waits while the user defines the category of the image presented. See
#				full description in README.md
# Copyright:	SpicaLab, 2015
# License:		MIT
#

# Default name for the config file
CONFIG_NAME="imgcat.conf"
# Root directory for categories
ROOT_DIR=""
# Full path to config file
CONFIG=""
# Directory for custom scripts' configs
SCRIPTBOX_CONFIGS_DIR="$HOME/.config/scriptbox"

# Array of keys
declare -a g_key
# Array of categories
declare -a g_category

print_help()
{
	local name

	name=$(basename "$0")
	echo "NAME"
	echo -e "\t$name is a simple shell script used to facilitate sorting of a set of images"
	echo "SYNOPSIS"
	echo -e "\t$name [-h][PATH]"
	echo "DESCRIPTION"
	echo -e "\tThis script allows user to sort all the images in current directory according to categories defined in "
	echo -e "\tconfig file. The script goes through all the images one by one, shows them with feh and presents a menu "
	echo -e "\tcontaning the categories specified in config file. User has an option to define the category of an image "
	echo -e "\tpresented or skip it. The script then creates a symlink for image in a directory named after category."
	echo "OPTIONS"
	echo -e "\t-h"
	echo -e "\t\tDisplay this help and exit"
	echo -e "\tPATH"
	echo -e "\t\tPath to a directory where subdirectories for each category will be created. Subdirectories are created "
	echo -e "\t\tin current directory in case this path is not specified."
}

check_prerequisities()
{
	local dmenu_present feh_present

	dmenu_present=$(which dmenu)
	feh_present=$(which feh)
	if [[ -z "$dmenu_present" || -z "$feh_present" ]]; then
		echo "feh and dmenu should be installed in order to proceed"
		exit 1
	fi
}

# Create default config file. This function expects a variable to which the
# path will be saved
default_config()
{
	local cfg_file
	local __result="$1"
	if [[ -d "$HOME/.config" ]]; then
		if [[ ! -d "$SCRIPTBOX_CONFIGS_DIR" ]]; then
			mkdir "$SCRIPTBOX_CONFIGS_DIR"
		fi
		cfg_file="$SCRIPTBOX_CONFIGS_DIR/$CONFIG_NAME"
	else
		cfg_file="$HOME/.$CONFIG_NAME"
	fi
	{ echo "f:family"; echo "n:nature"; echo "p:pets:"; } >> "$cfg_file"

	# shellcheck disable=SC2086
	eval $__result="'$cfg_file'"
}

# This function expects a path to config file as first argument
read_config()
{
	local line comment key category

	if [[ -z "$1" ]]; then
		echo "Error: read_config() is invoked without the path to config file"
		exit 1
	fi

	# Add two special cases to global arrays: skip and quit
	g_key[${#g_key[*]}]="s"
	g_key[${#g_key[*]}]="q"
	g_category[${#g_category[*]}]="skip this image"
	g_category[${#g_category[*]}]="quit"

	# Add categories from config file, ignore lines starting with #
	IFS=$'\n'
	for line in $(< "$1"); do
		comment=$(echo "$line" | grep "^#")
		if [[ -z "$comment" && "x$line" != "x" ]]; then
			key=$(echo "$line" | cut -d ':' -f1)
			category=$(echo "$line" | cut -d ':' -f2)
			if [[ ! -z "$key" ]] && [[ ! -z "$category" ]]; then
				g_key[${#g_key[*]}]="$key"
				g_category[${#g_category[*]}]="$category"
			fi
		fi
	done
	unset IFS
}

# Check directories specified in g_category list
check_dirs()
{
	local trailing_slash dir

	trailing_slash=$(echo "$ROOT_DIR" | grep -q "/$")
	if [[ ! "$trailing_slash" ]]; then
		ROOT_DIR="$ROOT_DIR/"
	fi
	# The first two categories have special meaning thus this loop starts from the third element
	for dir in "${g_category[@]:2}"; do
		if [[ ! -d "$ROOT_DIR$DIR" ]]; then
			echo "Directory $ROOT_DIR$dir does not exist and will be created"
			mkdir "$ROOT_DIR$dir"
		fi
	done
}

# Parse command-line options
case "$#" in
	0)	ROOT_DIR="./"
		;;
	1)	if [ "$1" == "-h" ]; then
			print_help
			exit 0
		else
			if [[ ! -d "$1" ]]; then
				echo "The directory specified does not exist, check the path"
				exit 1
			else
				ROOT_DIR="$1"
			fi
		fi
		;;
	*)	print_help
		exit 0
		;;
esac

check_prerequisities
if [[ -f "$SCRIPTBOX_CONFIGS_DIR/$CONFIG_NAME" ]]; then
	CONFIG="$SCRIPTBOX_CONFIGS_DIR/$CONFIG_NAME"
else
	if [[ -f "$HOME/.$CONFIG_NAME" ]]; then
		CONFIG="$HOME/.$CONFIG_NAME"
	fi
fi
if [[ -z "$CONFIG" ]]; then
	default_config "$CONFIG"
fi
read_config "$CONFIG"
check_dirs

DIR=$(pwd)
for FILE in *; do
	FILE_TYPE=$(file --mime-type "$FILE" | grep "image")
	if [[ ! -z "$FILE_TYPE" ]]; then
		feh -F "$FILE" &
		FEH_PID=$!

		# Assemble items for dmenu
		declare -a script
		for index in ${!g_key[*]}; do
			script=("${script[@]}" "(${g_key[$index]})${g_category[$index]}")
		done
		SELECTION=$(for index in ${!g_key[*]}; do
			echo "${g_key[$index]} "
		done | dmenu -i -p "Define category for the image ${script[*]}:" | tr -d " ")

		# Process user's selection
		INDEX=0
		FOUND=0
		if [[ "$SELECTION" == "s" ]]; then
			kill -STOP $FEH_PID
			unset script
			continue
		else
			if [[ "$SELECTION" == "q" ]]; then
				kill -STOP $FEH_PID
				exit 0
			fi
		fi
		for ELEM in "${g_key[@]}"; do
			if [[ "$SELECTION" == "$ELEM" ]]; then
				FOUND=1
				break
			fi
			(( INDEX+=1 ))
		done
		if [[ "$FOUND" == 1 ]]; then
			CURRENT_DIR="${g_category[$INDEX]}"
			POSTFIX=$(date +%N)
			ln -s -b -r "$FILE" "$ROOT_DIR$CURRENT_DIR/$FILE-$POSTFIX"
		fi

		# Clean up
		kill -STOP $FEH_PID
		unset script
	fi
done
